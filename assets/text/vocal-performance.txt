During my high school years, I stumbled upon rap music, slam poetry, and various
vocal performance arts. Instantly captivated by their modern and liberating
writing styles, I found myself drawn into a world of creative expression. While
I had already dabbled in wordplay and rhyming techniques, delving deeper into
the works of my favorite artists introduced me to a wealth of new techniques and
inspirations, enriching my understanding and appreciation of these art forms.

My passion for vocal performance arts led me to participate in an eloquence
contest, where I was tasked with proposing and defending a promotional name for
my student residence. This experience taught me a valuable lesson: speaking
eloquently is not enough; understanding your audience is paramount, especially
when aiming to impress rather than merely inform.