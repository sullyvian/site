function copyMail() {
    var mailText = "vian.hervy@gmail.com";
    navigator.clipboard
        .writeText(mailText)
        .then(function () {
            alert("Copied the text: " + mailText);
        })
        .catch(function (error) {
            alert("Error copying text: " + error);
        });
}
