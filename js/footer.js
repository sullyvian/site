fetch('../html/footer.html')
    .then(response => response.text())
    .then(data => {
        // Select all <footer> elements and set the innerHTML for each
        document.querySelectorAll('footer').forEach(footer => {
            footer.innerHTML = data;
        });
    })
    .catch(error => {
        console.error('Error fetching footer:', error);
    });
