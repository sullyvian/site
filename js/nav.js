// importing nav.html to all pages
fetch("../html/nav.html")
    .then((response) => response.text())
    .then((data) => {
        // Select all <footer> elements and set the innerHTML for each
        document.querySelectorAll("nav").forEach((footer) => {
            footer.innerHTML = data;
        });
    })
    .catch((error) => {
        console.error("Error fetching footer:", error);
    });

// to copy email address
function copyMail() {
    var mailText = "vian.hervy@gmail.com";
    navigator.clipboard
        .writeText(mailText)
        .then(function () {
            alert("Email address (" + mailText + ") has been copied to clipboard!");
        })
        .catch(function (error) {
            alert("Error copying text: " + error);
        });
}

function unfinished () {
    alert("This page is still under construction!");
}