function fetchParagraphs(id) {
    fetch(`../assets/text/${id}.txt`)
        .then(response => response.text())
        .then(data => {
            // Replace double newline character with <br>
            const formattedData = data.replace(/\n\n/g, '<br><br>');
            // Insert the text into the corresponding HTML element
            document.getElementById(id).innerHTML = formattedData;
        });
}

const ids = ["scoutism", "rock-climbing", "vocal-performance"];

ids.forEach(fetchParagraphs);