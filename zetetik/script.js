const imgDir = './assets/images/';
const audioDir = './assets/audios/'; // pas bon yet

let lastClickTime = null;
let csvContent = 'data:text/csv;charset=utf-8,Music Name, Image Name,Elapsed Time (ms),Button\n';

let clickCount = 0;
const maxClickCount = 25;

const waitTime = 10_000;

let buttons;
let imgButton;

let imgName = getRandomImageName();
let audioName = getRandomAudioName();
let audio = new Audio(`${audioDir}${audioName}`);

window.onload = function () {

    buttons = document.getElementsByClassName('button');
    for (let i = 0; i < buttons.length; i++) {
        buttons[i].style.cursor = 'not-allowed';
    }
    imgButton = document.getElementById('img-button');
    imgButton.addEventListener('click', startExperience);

    // keyboard listener
    document.addEventListener('keydown', function (event) {
        if (event.key === 'ArrowLeft') {
            buttons[0].click();
        } else if (event.key === 'ArrowRight') {
            buttons[1].click();
        } else if (event.key === 'Enter' || event.key === ' ') {
            imgButton.click();
        }
    });
}

async function startExperience() {
    audio.play();
    imgButton.removeEventListener('click', startExperience);
    imgButton.textContent = 'Pour chaque image, cliquez sur le bouton qui correspond le mieux selon vous à l\'image affichée.';
    imgButton.style.cursor = 'not-allowed';

    // attendre
    await new Promise(r => setTimeout(r, waitTime));

    imgButton.textContent = '';
    imgButton.style.backgroundImage = `url("${imgDir}${imgName}")`;

    for (let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', changeImage);
        buttons[i].style.cursor = 'pointer';
    }

    lastClickTime = new Date();
}

function getRandomAudioName() {
    const audioIndex = Math.floor(Math.random() * audios.length);
    return audios[audioIndex];
}

function getRandomImageName() {
    const imgIndex = Math.floor(Math.random() * images.length);
    return images[imgIndex];
}

function changeImage(event) {
    // Write elapsed time
    if (lastClickTime) {
        const buttonClicked = event.target;
        clickCount++;
        const currentTime = new Date();
        const elapsedTime = currentTime - lastClickTime;
        console.log('Elapsed time: ' + elapsedTime + 'ms');
        lastClickTime = currentTime;
        writeCsv(audioName, imgName, elapsedTime, buttonClicked.id);
    }

    // Change image
    imgName = getRandomImageName();
    imgButton.style.backgroundImage = `url("${imgDir}${imgName}")`;

    // End experience
    if (clickCount >= maxClickCount) {
        endExperience();
        audio.pause();
        audio.currentTime = 0;
    }
}

function endExperience() {
    const imgButton = document.getElementById('img-button');
    imgButton.textContent = 'Cliquer pour télécharger le CSV';
    imgButton.style.backgroundImage = 'none';
    imgButton.style.cursor = 'pointer';
    imgButton.addEventListener('click', downloadCsv);

    for (let i = 0; i < buttons.length; i++) {
        buttons[i].removeEventListener('click', changeImage);
        buttons[i].style.cursor = 'not-allowed';
    }
}

function downloadCsv() {
    const encodedUri = encodeURI(csvContent);
    const link = document.createElement('a');
    const timestamp = new Date().toISOString().replace(/[:. ]/g, '-');
    link.setAttribute('href', encodedUri);
    link.setAttribute('download', `oasis-${timestamp}.csv`);
    document.body.appendChild(link);
    link.click();
    imgButton.removeEventListener('click', downloadCsv);
    imgButton.textContent = 'Merci !\nCliquez pour recommencer';
    imgButton.style.cursor = 'pointer';
    imgButton.addEventListener('click', function() {
        window.location.reload();
    });
}

function writeCsv(audioName, imgName, elapsedTime, buttonId) {
    const string = `${audioName},${imgName},${elapsedTime},${buttonId}`;
    csvContent += string + '\n';
    console.log(string);
}
