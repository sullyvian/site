#!/bin/bash

# specify the directory
imgDir="./assets/images/"

# start the JavaScript array
echo "const images = ["

# loop through the files in the directory
for file in "$imgDir"*
do
    # print the filename, surrounded by quotes and followed by a comma
    echo "\"${file#"$imgDir"}\","
done

# end the JavaScript array
echo "];"

audioDir="./assets/audios/"

echo "const audios = ["

for file in "$audioDir"*
do
    echo "\"${file#"$audioDir"}\","
done

echo "];"

# for merging all csv
# tail -n +2 -q ./results/singles/*.csv > ./results/total.csv